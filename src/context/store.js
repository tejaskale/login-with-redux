import { configureStore } from "@reduxjs/toolkit";
import  tableSlice  from "./rootReducer"

export default configureStore({
    reducer:{
        userTable: tableSlice ,
    },
})