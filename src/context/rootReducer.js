import {createSlice} from "@reduxjs/toolkit"

export const tableSlice = createSlice({
    name:'UserTable',
    initialState:{
        users:[],
        editUser:{}
    },

    reducers:{
        add : (state,action)=>{
            return{...state,users:action.payload}
        },
        edit : (state,action)=>{
            return{...state, editUser:action.payload}
        }
    },
})
export let { add , edit} = tableSlice.actions;

export default tableSlice.reducer;