import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom';
import "./Table.styles.css"
import {edit,add} from "../context/rootReducer"

function Table() {
    let [items,setItems]=useState(useSelector((state)=>state.userTable.users))
    let [clickedOnDelete,setClickedOnDelete] = useState(false)
    console.log(useSelector((state)=>state.userTable.users));
    let dispatch = useDispatch()



    function handleEdit(value){
      let editedFiltered =items.filter((user,id)=>{
        return(
          user!==value
        )
      })
      
      dispatch(add(editedFiltered))
      dispatch(edit(value))
    }

    function handleDelete(value){
      let deletedFilter = items.filter((user,id)=>{
        return(
          user!==value
        )
      })
      dispatch(add(deletedFilter))
      setItems(deletedFilter)
    }
    
  return (
    <div className='' >
       <table>
    <thead>
      <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Password</th>
        <th>City</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      {items.map((value,id)=>{
            return(
                <tr key={id} >
                    <td>{value.name}</td>
                    <td>{value.mail}</td>
                    <td>{value.pass}</td>
                    <td>{value.city}</td>
                    <td>
                    <Link to="/" >
                    <button className='btn' onClick={()=>{handleEdit(value) ;}}>Edit</button>
                    </Link>
                    <button className='btn' onClick={()=>{handleDelete(value) ; setClickedOnDelete(!clickedOnDelete)}} >Delete</button> 
                    </td>
                </tr>
            )
      })}
    </tbody>
  </table>
  <div className="btn-container">
  <Link to="/" >
            <button className='table-btn'>Back</button>
  </Link>
  </div>
    </div>
  )
}

export default Table