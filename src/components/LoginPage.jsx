import React, { Fragment, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { add } from '../context/rootReducer'
import { Link } from 'react-router-dom'
import "./LoginPage.Styles.css"


function LoginPage() {
    let editdata = useSelector((state)=>state.userTable.editUser)
    console.log(editdata);

    let [name,setName]=useState(editdata.name || "")
    let [mail,setmail]= useState(editdata.mail || "")
    let [pass,setPass]=useState(editdata.pass || "")
    let [city,setCity]=useState(editdata.city || "")
    let [isValid,setIsValid] = useState(false)


    let myName = useRef()
    let myMail = useRef()
    let myPass = useRef()
    let myCity = useRef()

    console.log(myName);

    let dispatch = useDispatch()
    let data = useSelector((state)=>state.userTable.users);

    
    function handleSave(){
        let userData={
            name:name,
            mail:mail,
            pass:pass,
            city:city
        }
        let allData=[...data,userData]
        dispatch(add(allData));

        setCity("")
        setName("")
        setPass("")
        setmail("")
    }


  return (
    <Fragment>
        <div className="container">
        <div className="card">
            <h2 className='heading' >SignUp Page</h2>
        <div>
            <input type="text" className='inp-tag' value={name} ref={myName} onChange={(e)=>{setName(e.target.value) ; name.length >=4 ? myName.current.style.border = "3px solid  rgb(22, 233, 22)": myName.current.style.border = "3px solid red"}} placeholder='Enter Your Name' />
        
        </div>
        <div>
            <input type="email" className='inp-tag' ref={myMail} value={mail} onChange={(e)=>{setmail(e.target.value) ; mail.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/) ? myMail.current.style.border = "3px solid  rgb(22, 233, 22)": myMail.current.style.border = "3px solid red"}} placeholder='Enter Your email' />
        </div>
        <div>
            <input type="password" className='inp-tag' ref={myPass} value={pass} onChange={(e)=>{setPass(e.target.value) ; pass.length >=4 ? myPass.current.style.border = "3px solid  rgb(22, 233, 22)": myPass.current.style.border = "3px solid red"}} placeholder='Enter Your Password' />
        </div>
        <div>
            <input type="text" className='inp-tag' ref={myCity} value={city} onChange={(e)=>{setCity(e.target.value) ; city.length >=4 ? myCity.current.style.border = "3px solid  rgb(22, 233, 22)": myCity.current.style.border = "3px solid red"}} placeholder='Enter Your city' />
        </div>
        <div>
        <button className='btn'  onClick={handleSave}>Save</button>
        
            <Link to="/table" >
            <button className='btn' >Table</button>
            </Link>
        
        </div>
        </div>
        </div>
    </Fragment>
  )
}

export default LoginPage