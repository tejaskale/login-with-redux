import logo from './logo.svg';
import './App.css';
import LoginPage from './components/LoginPage';
import Table from './components/Table';
import { BrowserRouter, Routes, Route } from "react-router-dom";

function App() {
  return (
    <div>
      {/* <LoginPage/>
      <Table/> */}

      <BrowserRouter>
      <Routes>

          <Route path='/' element={<LoginPage/>} />
          <Route path="/table" element={<Table/>} />
        
      </Routes>
    </BrowserRouter>
    </div>
  );
}

export default App;
